package ru.dolbak.dbjava;

import androidx.appcompat.app.AppCompatActivity;

import android.content.res.AssetManager;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ListView;
import android.widget.SimpleAdapter;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class MainActivity extends AppCompatActivity {

    String SQL_request = "SELECT `tracks`.`Name` as `Track Name`, `tracks`.`Composer` as `Composer`, `albums`.`Title` as `Album`, `genres`.`Name` as `Genre`, `media_types`.`Name` as `Media type` FROM `tracks`\n" +
            "LEFT JOIN `albums` ON `albums`.`AlbumId` = `tracks`.`AlbumId`\n" +
            "LEFT JOIN `genres` ON `genres`.`GenreId` = `tracks`.`GenreId`\n" +
            "LEFT JOIN `media_types` ON `media_types`.`MediaTypeId` = `tracks`.`MediaTypeId`;";

    String SQL_request_ASC = "SELECT `tracks`.`Name` as `Track Name`, `tracks`.`Composer` as `Composer`, `albums`.`Title` as `Album`, `genres`.`Name` as `Genre`, `media_types`.`Name` as `Media type` FROM `tracks`\n" +
            "LEFT JOIN `albums` ON `albums`.`AlbumId` = `tracks`.`AlbumId`\n" +
            "LEFT JOIN `genres` ON `genres`.`GenreId` = `tracks`.`GenreId`\n" +
            "LEFT JOIN `media_types` ON `media_types`.`MediaTypeId` = `tracks`.`MediaTypeId` ORDER BY `tracks`.`Name`;";
    String SQL_request_DESC = "SELECT `tracks`.`Name` as `Track Name`, `tracks`.`Composer` as `Composer`, `albums`.`Title` as `Album`, `genres`.`Name` as `Genre`, `media_types`.`Name` as `Media type` FROM `tracks`\n" +
            "LEFT JOIN `albums` ON `albums`.`AlbumId` = `tracks`.`AlbumId`\n" +
            "LEFT JOIN `genres` ON `genres`.`GenreId` = `tracks`.`GenreId`\n" +
            "LEFT JOIN `media_types` ON `media_types`.`MediaTypeId` = `tracks`.`MediaTypeId` ORDER BY `tracks`.`Name` DESC;";


    ListView listView;

    String[] from = {"track_name", "composer"};
    int[] to = {R.id.textView, R.id.textView2};
    ArrayList<Map<String, Object>> data;
    Map<String, Object> m;
    SimpleAdapter simpleAdapter;
    SQLiteDatabase db;
    DataBaseHelper dataBaseHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        dataBaseHelper = new DataBaseHelper(this);
        dataBaseHelper.openDatabase();
        db = dataBaseHelper.getReadableDatabase();
        Cursor cursor = db.rawQuery(SQL_request, null);


        data = new ArrayList<>();

        while (cursor.moveToNext()) {
            //Log.d("CURSOR", cursor.getString(1));
            m = new HashMap<String, Object>();
            m.put("track_name", cursor.getString(0));
            m.put("composer", cursor.getString(1));
            data.add(m);
        }

        simpleAdapter = new SimpleAdapter(this, data, R.layout.list_view_layout,
                from, to);

        listView = findViewById(R.id.list_view);
        listView.setAdapter(simpleAdapter);
    }

    public void onClick1(View view) {
        Cursor cursor = db.rawQuery(SQL_request_ASC, null);
        data.clear();
        while (cursor.moveToNext()) {
            //Log.d("CURSOR", cursor.getString(1));
            m = new HashMap<String, Object>();
            m.put("track_name", cursor.getString(0));
            m.put("composer", cursor.getString(1));
            data.add(m);
        }
        simpleAdapter.notifyDataSetInvalidated();
    }

    public void onClick2(View view) {
        Cursor cursor = db.rawQuery(SQL_request_DESC, null);
        data.clear();
        while (cursor.moveToNext()) {
            //Log.d("CURSOR", cursor.getString(1));
            m = new HashMap<String, Object>();
            m.put("track_name", cursor.getString(0));
            m.put("composer", cursor.getString(1));
            data.add(m);
        }
        simpleAdapter.notifyDataSetInvalidated();
    }
}