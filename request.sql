SELECT `tracks`.`Name` as `Track Name`, `tracks`.`Composer` as `Composer`, `albums`.`Title` as `Album`, `genres`.`Name` as `Genre`, `media_types`.`Name` as `Media type` FROM `tracks`
LEFT JOIN `albums` ON `albums`.`AlbumId` = `tracks`.`AlbumId`
LEFT JOIN `genres` ON `genres`.`GenreId` = `tracks`.`GenreId`
LEFT JOIN `media_types` ON `media_types`.`MediaTypeId` = `tracks`.`MediaTypeId`
LIMIT 100;